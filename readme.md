1. Run gen_modelnet_pickle.ipynb or gen_oowl_pickle.ipynb to generate the pickle file for your dataset. The pregenerated pickle file is provied

    Pickle file is organized as
    a. train --> class --> objs (each obj has 12 views)
                       --> labels (the class label for the object)

    b. test --> class --> objs (each obj has 12 views)
                      --> labels (the class label for the object)
                      
2.  Run net.py to train the mvcnn model. The architecture of mvcnn is written in mvcnn.py, while the dataloader is written in data_processing.py. The trained model will be stored in model folder. The classification accuracy can be seen in the log folder.                   

3.  Run net_cls_rtr.py.py to compute the feature. The feature is extracted from right after the view pooling operations. More details can be seen in mvcnn_retreival_class.py

4.  Run object_retreival.ipynb for object retrieval task. The MAP and precision recall curve is computed. The top 10 retrieved objects is shown as well

(Optional)
5.  Run mahalanobis_dist.py to compute the low rank mahalanobis metric.
4.  Run object_retreival_with_metric.ipynbb for object retrieval task with the low rank mahalanobis metric. The MAP and precision recall curve is computed. The top 10 retrieved objects is shown as well. Note that this accuracy should be better the MAP without metric.