import torch.nn as nn
import torch.nn.functional as F
from torch.nn import init
import torch
from torchvision.models.alexnet import model_urls
from torchvision.models.alexnet import model_urls as model_url_alexnet
from torchvision.models.vgg import model_urls as model_url_vgg
import torchvision.models as models
import random

# use element wise max pool during the view pooling stage
class mvcnn_Alex_pool_class_rtr(nn.Module):
    def __init__(self, pretrained_model, input_view, output_class):
        super(mvcnn_Alex_pool_class_rtr, self).__init__()
        pretrained_model.eval()
        self.input_view = pretrained_model.input_view
        self.net = pretrained_model.net
        self.input_view = input_view
        self.output_class = output_class

        
    def forward(self, x):
        # x: (n, self.view, 3, 224, 224) 
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.max(merge_result,view_feature)
                
        merge_result = merge_result.view(merge_result.size(0), -1)            

        return merge_result

# use avg during the view pooling stage
class mvcnn_Alex_avg_class_rtr(nn.Module):
    def __init__(self, pretrained_model, input_view, output_class):
        super(mvcnn_Alex_avg_class_rtr, self).__init__()        
        pretrained_model.eval()
        self.input_view = pretrained_model.input_view
        self.net = pretrained_model.net
        self.input_view = input_view
        self.output_class = output_class
        self.classifier = pretrained_model.classifier

    def forward(self, x): 
        # x: (n, self.view, 3, 224, 224)     
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.cat([merge_result, view_feature],1)
                
        # append and shrink down number of features
        merge_result = torch.mean(merge_result,1)        
        merge_result = merge_result.view(merge_result.size(0), -1)      
        return merge_result  
    
    
# use element wise max pool during the view pooling stage
class mvcnn_VGG_pool_class_rtr(nn.Module):
    def __init__(self, pretrained_model, input_view, output_class):
        super(mvcnn_VGG_pool_class_rtr, self).__init__()        
        pretrained_model.eval()
        self.input_view = pretrained_model.input_view
        self.net = pretrained_model.net
        self.input_view = input_view
        self.output_class = output_class
        self.classifier = pretrained_model.classifier
        
    def forward(self, x):
        # x: (n, self.view, 3, 224, 224) 
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.max(merge_result,view_feature)
                
        merge_result = merge_result.view(merge_result.size(0), -1)        
        return merge_result

# use avg during the view pooling stage
class mvcnn_VGG_avg_class_rtr(nn.Module):
    def __init__(self, pretrained_model, input_view, output_class):
        super(mvcnn_VGG_avg_class_rtr, self).__init__()        
        pretrained_model.eval()
        self.input_view = pretrained_model.input_view
        self.net = pretrained_model.net
        self.input_view = input_view
        self.output_class = output_class
        self.classifier = pretrained_model.classifier
        
    def forward(self, x): 
        # x: (n, self.view, 3, 224, 224)     
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.cat([merge_result, view_feature],1)
                
        # append and shrink down number of features
        merge_result = torch.mean(merge_result,1)        
        merge_result = merge_result.view(merge_result.size(0), -1)      
        return merge_result  