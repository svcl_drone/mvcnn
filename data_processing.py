import torch
import random
import numpy as np
import time
from PIL import Image
from random import randint

class Multiview_obj:
    def __init__(self, view_list, transform, num_of_view):
        # number of views
        self.V = num_of_view
        self.transform = transform
        # a list of views for a single object
        self.views = self._load_views(view_list, num_of_view)
       
    def _load_views(self, view_list, V):
        views = None
        sampled_view_list = np.random.choice(view_list, V, replace=False)
        for f in sampled_view_list:
            img = Image.open(f)
            if self.transform is not None:
                img = self.transform(img)
             
            img = img.unsqueeze(0)    
            if views is None:
                views = img
            else:
                views = torch.cat([views, img], 0)
        # views has shape [batchsize, # of view , 3, 224, 224]
        return views


class Dataset:
    def __init__(self, data, shuffle_f , V, transform = None, view_drop_out = False, num_of_view = None):
        self.data = data
        self.objs = data['objs']
        self.labels = data['labels']
        self.shuffle_f = shuffle_f
        self.V = V
        self.transform = transform
        self.view_drop_out = view_drop_out
        self.num_of_view = num_of_view
        if self.shuffle_f:
            self.shuffle()

    def size(self):
        return len(self.objs)

    def shuffle(self):
        z = zip(self.objs, self.labels)
        random.shuffle(z)
        self.objs, self.labels = [list(l) for l in zip(*z)]
        
    def preprocess(self, views_for_a_single_object, num_of_view):
        s = Multiview_obj(views_for_a_single_object, self.transform, num_of_view)           
        return s
    
    def batches(self, batch_size):
        objs = self.objs
        labels = self.labels
        n = len(objs)
        if self.shuffle_f:
            self.shuffle()
        for i in xrange(0, n, batch_size):
            objs_batch = objs[i: i + batch_size]            
            labels_batch = labels[i: i + batch_size]
            
            # view drop out
            if self.view_drop_out:
                if self.num_of_view is None:
                    self.num_of_view = randint(1, self.V)
            else: 
                self.num_of_view = self.V
            
            x = torch.zeros(len(objs_batch), self.num_of_view, 3, 224, 224)
            y = torch.zeros(len(objs_batch)).type(torch.LongTensor)            
            index = 0
           
                
            for obj in objs_batch:         
                shape_object = self.preprocess(obj, self.num_of_view)                
                x[index] = shape_object.views
                y[index] = labels_batch[index]
                index += 1

            yield x, y