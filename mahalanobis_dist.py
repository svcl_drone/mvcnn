from __future__ import print_function, division
from pylmnn.lmnn import LargeMarginNearestNeighbor as LMNN
import numpy as np
import pickle
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
import matplotlib
matplotlib.use('agg')
from matplotlib import cm, pyplot as plt
import matplotlib.font_manager as font_manager
path = 'times.ttf'
prop = font_manager.FontProperties(fname=path)
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import torchvision
from torchvision import datasets, models, transforms
import copy
import torchvision.models as models
import requests
import argparse
import logging
from torch.utils.data import DataLoader
from torch.autograd import Variable
import torchvision.transforms as transforms
import numpy as np
import time
import os
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
from data_processing import Dataset
import os
import torch.cuda as cuda
import pickle


parser = argparse.ArgumentParser(description='PyTorch Training')
parser.add_argument("--net", default='AlexNet_pool', const='AlexNet_pool',nargs='?', choices=['AlexNet_pool', 'AlexNet_avg', 'VGG_pool', 'VGG_avg'], help="net model(default:AlexNet)")
parser.add_argument("--dataset", default='ModelNet', const='ModelNet',nargs='?', choices=['OOWL', 'ModelNet', 'OOWL_single_cpy'], help="Dataset (default:ModelNet)")
parser.add_argument('--k', action='store', default=5, type=int, help='knn (default: 5)')
parser.add_argument('--iter', action='store', default=20, type=int, help='iter (default: 20)')
arg = parser.parse_args()

# load dataset
def load_data(pickle_filename):

    train = {}
    train['objs'] = []
    train['labels'] = []
    test = {}
    test['objs'] = []
    test['labels'] = []
    with open(pickle_filename, "rb") as input_file:
        data = pickle.load(input_file)    
    
    for c in data['train']:
        for obj in data['train'][c]:
            train['objs'].append(data['train'][c][obj]['objs'])
            train['labels'].append(data['train'][c][obj]['labels'])
            
    for c in data['test']:
        for obj in data['test'][c]:
            test['objs'].append(data['test'][c][obj]['objs'])
            test['labels'].append(data['test'][c][obj]['labels'])
            
    return train, test



def main():
    config = arg.net + '_' + arg.dataset + '_' + str(arg.k) + 'nn_' + str(arg.iter) + 'iter'
    print(config)
    
    train_feature = np.load("train_feature_npy/"+ arg.net + '_' + arg.dataset + ".npy")
    
    # Read list of training and validation data
    if arg.dataset == 'OOWL':
        pickle_filename = 'oowl_sorted.pickle'
    elif arg.dataset == 'ModelNet':
        pickle_filename = 'modelnet_sorted.pickle'
    elif arg.dataset == 'OOWL_single_cpy':
        pickle_filename = 'oowl_sorted_single_copy.pickle'
        
    train, test = load_data(pickle_filename)
    if arg.dataset == 'OOWL' or arg.dataset == 'OOWL_single_cpy':
        dataset_train = Dataset(train, shuffle_f=False, V=8)
    elif arg.dataset == 'ModelNet':
        dataset_train = Dataset(train, shuffle_f=False, V=12) 
    
    
    train_label = np.asarray(dataset_train.data['labels'])
    # Set up the hyperparameters
    k_tr, dim_out, max_iter = arg.k, 128, arg.iter

    print('k_tr: ' + str(k_tr))
    print('max_iter: ' + str(max_iter))
    # Instantiate the classifier
    clf = LMNN(n_neighbors=k_tr, max_iter=max_iter, n_features_out=dim_out)

    # Train the classifier
    clf = clf.fit(train_feature, train['labels'])
    train_feature_trans = clf.transform(train_feature)

    
    if not os.path.exists("mahalanobis_dist"):
        os.mkdir("mahalanobis_dist")
    

    np.save("mahalanobis_dist/" + config +"_L.npy" , clf.L_)

    check_L = np.load("mahalanobis_dist/" + config +"_L.npy")
    train_feature_trans_check = train_feature.dot(check_L.T)
    print(np.sum(train_feature_trans - train_feature_trans_check))
    
if __name__ == "__main__":
    main()