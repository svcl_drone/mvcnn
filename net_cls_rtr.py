from __future__ import print_function, division
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn

import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import torchvision
from torchvision import datasets, models, transforms
import copy
import torchvision.models as models
import requests
import argparse
import logging
from torch.utils.data import DataLoader
from torch.autograd import Variable
import torchvision.transforms as transforms
import numpy as np
import time
import os
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
from mvcnn import mvcnn_Alex_pool
from mvcnn import mvcnn_Alex_avg
from mvcnn import mvcnn_VGG_pool
from mvcnn import mvcnn_VGG_avg
from mvcnn_retreival_class import mvcnn_Alex_pool_class_rtr
from mvcnn_retreival_class import mvcnn_Alex_avg_class_rtr
from mvcnn_retreival_class import mvcnn_VGG_pool_class_rtr
from mvcnn_retreival_class import mvcnn_VGG_avg_class_rtr
from data_processing import Dataset
import os
import torch.cuda as cuda
import pickle


parser = argparse.ArgumentParser(description='PyTorch Training')
parser.add_argument('-e', '--epochs', action='store', default=20, type=int, help='epochs (default: 40)')
parser.add_argument('--batchSize', action='store', default=8, type=int, help='batch size (default: 8)')
parser.add_argument('--lr', '--learning-rate', action='store', default=0.0001, type=float, help='learning rate (default: 0.0001)')
parser.add_argument('--m', '--momentum', action='store', default=0.9, type=float, help='momentum (default: 0.9)')
parser.add_argument('--w', '--weight-decay', action='store', default=0, type=float, help='regularization weight decay (default: 0.0)')
parser.add_argument('--train_f', action='store_false', default=True, help='Flag to train (STORE_FALSE)(default: True)')
parser.add_argument('--useGPU_f', action='store_false', default=True, help='Flag to use GPU (STORE_FALSE)(default: True)')
parser.add_argument('--preTrained_f', action='store_false', default=True, help='Flag to pretrained model (default: True)')
parser.add_argument('--gpu_num', action='store', default=0, type=int, help='gpu_num (default: 0)')
parser.add_argument("--net", default='AlexNet_pool', const='AlexNet_pool',nargs='?', choices=['AlexNet_pool', 'AlexNet_avg', 'VGG_pool', 'VGG_avg'], help="net model(default:AlexNet)")
parser.add_argument("--dataset", default='ModelNet', const='ModelNet',nargs='?', choices=['OOWL', 'ModelNet', 'OOWL_single_cpy'], help="Dataset (default:ModelNet)")
parser.add_argument("--sample_view", action='store', type=int, help='sample_view (1~ max view)')
parser.add_argument('--num_test', action='store', default=5, type=int, help='num_test (default: 5)')
arg = parser.parse_args()


# load dataset
def load_data(pickle_filename):

    train = {}
    train['objs'] = []
    train['labels'] = []
    test = {}
    test['objs'] = []
    test['labels'] = []
    with open(pickle_filename, "rb") as input_file:
        data = pickle.load(input_file)    
    
    for c in data['train']:
        for obj in data['train'][c]:
            train['objs'].append(data['train'][c][obj]['objs'])
            train['labels'].append(data['train'][c][obj]['labels'])
            
    for c in data['test']:
        for obj in data['test'][c]:
            test['objs'].append(data['test'][c][obj]['objs'])
            test['labels'].append(data['test'][c][obj]['labels'])
            
    return train, test


def main():
    # create model directory to store/load old model

    if not os.path.exists('log_gen_feature'):
        os.makedirs('log_gen_feature')

	# Logger Setting
    logger = logging.getLogger('netlog')
    logger.setLevel(logging.INFO)
    ch = logging.FileHandler('log_gen_feature/logfile_'+arg.net+'_'+arg.dataset +'.log')
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.info("================================================")
    logger.info("Learning Rate: {}".format(arg.lr))
    logger.info("Momentum: {}".format(arg.m))
    logger.info("Regularization Weight Decay: {}".format(arg.w))
    logger.info("Classifier: "+arg.net)
    logger.info("Dataset: "+arg.dataset)
    logger.info("Nbr of Epochs: {}".format(arg.epochs))
    logger.info("Sample_view: {}".format(arg.sample_view))
    # Batch size setting
    batch_size = arg.batchSize
    
    # load the data
    data_transforms = {
        'train': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'val': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'test': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
    }
    
    #print(torch.cuda.device_count())
    torch.cuda.set_device(arg.gpu_num)
    torch.cuda.current_device()

    # dataset directory
    if arg.dataset == 'ModelNet':
        '''
        pickle file is organized as
        1. train --> class --> objs (each obj has 12 views)
                           --> labels (the class label for the object)

        2. test --> class --> objs (each obj has 12 views)
                          --> labels (the class label for the object)
        '''
        pickle_filename = 'modelnet.pickle'
        
        # Read list of training and validation data
        train, test = load_data(pickle_filename)    
        output_class = 40
        input_view = 12
        dataset_train = Dataset(train, shuffle_f=False, V=input_view, transform =data_transforms['train'])
        dataset_test = Dataset(test, shuffle_f=False, V=input_view, transform =data_transforms['test'])
        
        
    elif arg.dataset == 'OOWL':
        '''
        pickle file is organized as
        1. train --> class --> objs (each obj has 8 views)
                           --> labels (the class label for the object)

        2. test --> class --> objs (each obj has 8 views)
                          --> labels (the class label for the object)
        '''
        pickle_filename = 'oowl_sorted.pickle'
        
        # Read list of training and validation data
        output_class = 23
        input_view = 8
        train, test = load_data(pickle_filename)    
        dataset_train = Dataset(train, shuffle_f=False, V=input_view, transform =data_transforms['train'])
        dataset_test = Dataset(test, shuffle_f=False, V=input_view, transform =data_transforms['test'])

    elif arg.dataset == 'OOWL_single_cpy':
        '''
        pickle file is organized as
        1. train --> class --> objs (each obj has 8 views)
                           --> labels (the class label for the object)

        2. test --> class --> objs (each obj has 8 views)
                          --> labels (the class label for the object)
        '''
        pickle_filename = 'oowl_sorted_single_copy.pickle'
        
        # Read list of training and validation data
        output_class = 23
        input_view = 8
        train, test = load_data(pickle_filename)    
        dataset_train = Dataset(train, shuffle_f=False, V=input_view, transform =data_transforms['train'])
        dataset_test = Dataset(test, shuffle_f=False, V=input_view, transform =data_transforms['test'])           
        
    logger.info("Output classes: {}".format(output_class))
    logger.info("Input view: {}".format(input_view))
    
    model_path = 'model/model_'+arg.net+'_'+arg.dataset+'.pt'
    if arg.net == 'AlexNet_pool':
        mvcnn_pretrained = mvcnn_Alex_pool(input_view, output_class)
        if os.path.isfile(model_path):
            mvcnn_pretrained.load_state_dict(torch.load(model_path))
        mvcnn_pretrained.eval()
        model_class_rtr = mvcnn_Alex_pool_class_rtr(mvcnn_pretrained, input_view, output_class)
    elif arg.net == 'AlexNet_avg':
        mvcnn_pretrained = mvcnn_Alex_avg(input_view, output_class)
        if os.path.isfile(model_path):
            mvcnn_pretrained.load_state_dict(torch.load(model_path))
        mvcnn_pretrained.eval()
        model_class_rtr = mvcnn_Alex_avg_class_rtr(mvcnn_pretrained, input_view, output_class)
    elif arg.net == 'VGG_pool':
        mvcnn_pretrained = mvcnn_VGG_pool(input_view, output_class)
        if os.path.isfile(model_path):
            mvcnn_pretrained.load_state_dict(torch.load(model_path))
        mvcnn_pretrained.eval()
        model_class_rtr = mvcnn_VGG_pool_class_rtr(mvcnn_pretrained, input_view, output_class)
    elif arg.net == 'VGG_avg':
        mvcnn_pretrained = mvcnn_VGG_avg(input_view, output_class)
        if os.path.isfile(model_path):
            mvcnn_pretrained.load_state_dict(torch.load(model_path))
        mvcnn_pretrained.eval()
        model_class_rtr = mvcnn_VGG_avg_class_rtr(mvcnn_pretrained, input_view, output_class)
        
        

    # for gpu mode
    if arg.useGPU_f:
        model_class_rtr.cuda()
    # for cpu mode
    else:
        model_class_rtr
    
    if not os.path.exists('train_feature_npy'):
        os.makedirs('train_feature_npy')
    if not os.path.exists('test_feature_npy'):
        os.makedirs('test_feature_npy')

    
    # Training
    print("Start Getting Training feature for Best Model")
    logger.info("Start Getting Training feature for Best Model")
    model_class_rtr.eval()
    step = 0
    train_feature = np.zeros((dataset_train.size(), 4096))
    for batch_x, batch_y in dataset_train.batches(batch_size):

        input, target = Variable(batch_x, volatile=True).cuda(), Variable(batch_y, volatile=True).cuda()

        # compute output
        output = model_class_rtr(input)
        
        idx = min(batch_size ,dataset_train.size()-step)
        train_feature[step:step+output.shape[0],:] = output[:idx].cpu().data.numpy()
        
        if step %100 == 0:
            print(step)
        step += output.shape[0]    
    np.save('train_feature_npy/'+ arg.net+'_'+arg.dataset +'.npy',train_feature)
    
    
    # Testing
    print("Start Getting Testing feature for Best Model")
    logger.info("Start Getting Testing feature for Best Model")
    model_class_rtr.eval()
    step = 0
    test_feature = np.zeros((dataset_test.size(), 4096))
    for batch_x, batch_y in dataset_test.batches(batch_size):

        input, target = Variable(batch_x, volatile=True).cuda(), Variable(batch_y, volatile=True).cuda()

        # compute output
        output = model_class_rtr(input)
        idx = min(batch_size ,dataset_test.size()-step)
        test_feature[step:step+output.shape[0],:] = output[:idx].cpu().data.numpy()
        
        if step %100 == 0:
            print(step)
        step += output.shape[0]
        
        
        
    np.save('test_feature_npy/'+ arg.net+'_'+arg.dataset +'.npy',test_feature)
    
    
if __name__ == "__main__":
    main()