from __future__ import print_function, division
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn

import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import torchvision
from torchvision import datasets, models, transforms
import copy
import torchvision.models as models
import requests
import argparse
import logging
from torch.utils.data import DataLoader
from torch.autograd import Variable
import torchvision.transforms as transforms
import numpy as np
import time
import os
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
from mvcnn import mvcnn_Alex_pool
from mvcnn import mvcnn_Alex_avg
from mvcnn import mvcnn_VGG_pool
from mvcnn import mvcnn_VGG_avg
from data_processing import Dataset
import os
import torch.cuda as cuda
import pickle


parser = argparse.ArgumentParser(description='PyTorch Training')
parser.add_argument('-e', '--epochs', action='store', default=20, type=int, help='epochs (default: 40)')
parser.add_argument('--batchSize', action='store', default=8, type=int, help='batch size (default: 8)')
parser.add_argument('--lr', '--learning-rate', action='store', default=0.0001, type=float, help='learning rate (default: 0.0001)')
parser.add_argument('--m', '--momentum', action='store', default=0.9, type=float, help='momentum (default: 0.9)')
parser.add_argument('--w', '--weight-decay', action='store', default=0, type=float, help='regularization weight decay (default: 0.0)')
parser.add_argument('--train_f', action='store_false', default=True, help='Flag to train (STORE_FALSE)(default: True)')
parser.add_argument('--useGPU_f', action='store_false', default=True, help='Flag to use GPU (STORE_FALSE)(default: True)')
parser.add_argument('--preTrained_f', action='store_false', default=True, help='Flag to pretrained model (default: True)')
parser.add_argument('--gpu_num', action='store', default=0, type=int, help='gpu_num (default: 0)')
parser.add_argument("--net", default='AlexNet_pool', const='AlexNet_pool',nargs='?', choices=['AlexNet_pool', 'AlexNet_avg', 'VGG_pool', 'VGG_avg'], help="net model(default:AlexNet)")
parser.add_argument("--dataset", default='ModelNet', const='ModelNet',nargs='?', choices=['OOWL', 'ModelNet', 'OOWL_single_cpy'], help="Dataset (default:ModelNet)")
arg = parser.parse_args()


# load dataset
def load_data(pickle_filename):

    train = {}
    train['objs'] = []
    train['labels'] = []
    test = {}
    test['objs'] = []
    test['labels'] = []
    with open(pickle_filename, "rb") as input_file:
        data = pickle.load(input_file)    
    
    for c in data['train']:
        for obj in data['train'][c]:
            train['objs'].append(data['train'][c][obj]['objs'])
            train['labels'].append(data['train'][c][obj]['labels'])
            
    for c in data['test']:
        for obj in data['test'][c]:
            test['objs'].append(data['test'][c][obj]['objs'])
            test['labels'].append(data['test'][c][obj]['labels'])
            
    return train, test


def main():
    # create model directory to store/load old model
    if not os.path.exists('model'):
        os.makedirs('model')
    if not os.path.exists('log'):
        os.makedirs('log')

	# Logger Setting
    logger = logging.getLogger('netlog')
    logger.setLevel(logging.INFO)
    ch = logging.FileHandler('log/logfile_'+arg.net+'_'+arg.dataset+'.log')
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.info("================================================")
    logger.info("Learning Rate: {}".format(arg.lr))
    logger.info("Momentum: {}".format(arg.m))
    logger.info("Regularization Weight Decay: {}".format(arg.w))
    logger.info("Classifier: "+arg.net)
    logger.info("Dataset: "+arg.dataset)
    logger.info("Nbr of Epochs: {}".format(arg.epochs))
    # Batch size setting
    batch_size = arg.batchSize
    
    # load the data
    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomResizedCrop(224, scale=(0.8, 1.0)),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'val': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'test': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
    }
    
    #print(torch.cuda.device_count())
    torch.cuda.set_device(arg.gpu_num)
    torch.cuda.current_device()

    # dataset directory
    if arg.dataset == 'ModelNet':
        '''
        pickle file is organized as
        1. train --> class --> objs (each obj has 12 views)
                           --> labels (the class label for the object)

        2. test --> class --> objs (each obj has 12 views)
                          --> labels (the class label for the object)
        '''
        pickle_filename = 'modelnet_sorted.pickle'
        
        # Read list of training and validation data
        train, test = load_data(pickle_filename)    
        output_class = 40
        input_view = 12
        if arg.train_f:
            dataset_train = Dataset(train, shuffle_f=True, V=input_view, transform =data_transforms['train'])
            dataset_val = Dataset(test, shuffle_f=True, V=input_view, transform =data_transforms['val'])
        dataset_test = Dataset(test, shuffle_f=True, V=input_view, transform =data_transforms['test'])
        
        
    elif arg.dataset == 'OOWL':
        '''
        pickle file is organized as
        1. train --> class --> objs (each obj has 8 views)
                           --> labels (the class label for the object)

        2. test --> class --> objs (each obj has 8 views)
                          --> labels (the class label for the object)
        '''
        pickle_filename = 'oowl_sorted.pickle'
        
        # Read list of training and validation data
        output_class = 23
        input_view = 8
        train, test = load_data(pickle_filename)    
        if arg.train_f:
            dataset_train = Dataset(train, shuffle_f=True, V=input_view, transform =data_transforms['train'])
            dataset_val = Dataset(test, shuffle_f=True, V=input_view, transform =data_transforms['val'])
        dataset_test = Dataset(test, shuffle_f=True, V=input_view, transform =data_transforms['test'])
    
    elif arg.dataset == 'OOWL_single_cpy':
        '''
        pickle file is organized as
        1. train --> class --> objs (each obj has 8 views)
                           --> labels (the class label for the object)

        2. test --> class --> objs (each obj has 8 views)
                          --> labels (the class label for the object)
        '''
        pickle_filename = 'oowl_sorted_single_copy.pickle'
        
        # Read list of training and validation data
        output_class = 23
        input_view = 8
        train, test = load_data(pickle_filename)    
        if arg.train_f:
            dataset_train = Dataset(train, shuffle_f=True, V=input_view, transform =data_transforms['train'])
            dataset_val = Dataset(test, shuffle_f=True, V=input_view, transform =data_transforms['val'])
        dataset_test = Dataset(test, shuffle_f=True, V=input_view, transform =data_transforms['test'])        
        
    logger.info("Output classes: {}".format(output_class))
    logger.info("Input view: {}".format(input_view))
    
    print(output_class)
    if arg.net == 'AlexNet_pool':
        model = mvcnn_Alex_pool(input_view, output_class)
    elif arg.net == 'AlexNet_avg':
        model = mvcnn_Alex_avg(input_view, output_class)
    elif arg.net == 'VGG_pool':
        model = mvcnn_VGG_pool(input_view, output_class)
    elif arg.net == 'VGG_avg':
        model = mvcnn_VGG_avg(input_view, output_class)
        
        
    optimizer = torch.optim.Adam(model.parameters(), lr=arg.lr)
    #optimizer = optim.SGD(model.parameters(), lr=arg.lr, momentum=arg.m)
    # for gpu mode
    if arg.useGPU_f:
        model.cuda()
    # for cpu mode
    else:
        model
    model_path = 'model/model_'+arg.net+'_'+arg.dataset+'.pt'

    # training
    print("Start Training")
    logger.info("Start Training")
    epochs = arg.epochs if arg.train_f else 0
    min_accuracy = 0
    
    
    if not os.path.exists('loss_npy'):
        os.makedirs('loss_npy')
    if not os.path.exists('acc_npy'):
        os.makedirs('acc_npy')
    
    loss_per_epoch_train = np.zeros(arg.epochs)
    loss_per_epoch_val = np.zeros(arg.epochs)
    acc_per_epoch_train = np.zeros(arg.epochs)
    acc_per_epoch_val = np.zeros(arg.epochs)
    
    
    correct, ave_loss = 0, 0
    for epoch in xrange(epochs):
        # training
        model.train()

        batch_idx = 0
        model.train()
        overall_acc = 0
        for batch_x, batch_y in dataset_train.batches(batch_size):
            optimizer.zero_grad()
            # for gpu mode
            if arg.useGPU_f:
                input, target = Variable(batch_x, requires_grad=True).cuda(), Variable(batch_y).cuda()
            # for cpu mode
            else:
                input, target = Variable(batch_x, requires_grad=True), Variable(batch_y)

            # use cross entropy loss
            criterion = nn.CrossEntropyLoss()
            
            outputs = model(input)
            
            # compute loss
            loss = criterion(outputs, target)
            
            _, pred_label = torch.max(outputs.data, 1)
            correct = (pred_label == target.data).sum()
            overall_acc += correct
            accuracy = 1.0*correct*1.0/batch_size
            loss.backward()              
            optimizer.step()             
            
            
            if batch_idx%50==0:
                print('==>>> epoch:{}, batch index: {}, train loss:{}, accuracy:{}'.format(epoch,batch_idx, loss.data[0], accuracy))
                logger.info('==>>> epoch:{}, batch index: {}, train loss:{}, accuracy:{}'.format(epoch,batch_idx, loss.data[0], accuracy))
            batch_idx += 1
            loss_per_epoch_train[epoch] += loss.data[0]
            acc_per_epoch_train[epoch] += float(correct)

            
        loss_per_epoch_train[epoch] /= (1.0*dataset_train.size())
        acc_per_epoch_train[epoch] /= (1.0*dataset_train.size())    
        
        # Validation (always save the best model)
        print("Start Validation")
        logger.info("Start Validation")
        
            # switch to evaluate mode
        model.eval()
        correct, ave_loss = 0, 0
        for batch_x, batch_y in dataset_val.batches(batch_size):
            # for gpu mode
            if arg.useGPU_f:
                input, target = Variable(batch_x, volatile=True).cuda(), Variable(batch_y, volatile=True).cuda()
            # for cpu mode
            else:
                input, target = Variable(batch_x, volatile=True), Variable(batch_y, volatile=True)

            # use cross entropy loss
            criterion = nn.CrossEntropyLoss()

            outputs = model(input)
            loss = criterion(outputs, target)
            _, pred_label = torch.max(outputs.data, 1)
            correct += (pred_label == target.data).sum()
            ave_loss += loss.data[0]

        accuracy = 1.0*correct*1.0/dataset_val.size()
        ave_loss /= dataset_val.size()
        if accuracy >= min_accuracy:
            min_accuracy = accuracy
            # save the model if it is better than current one
            torch.save(model.state_dict(), model_path)
        print('==>>> test loss:{}, accuracy:{}'.format(ave_loss, accuracy))
        logger.info('==>>> test loss:{}, accuracy:{}'.format(ave_loss, accuracy))

        loss_per_epoch_val[epoch] = ave_loss
        acc_per_epoch_val[epoch] = accuracy

        np.save('loss_npy/'+arg.net+'_'+arg.dataset + '_train', loss_per_epoch_train)
        np.save('loss_npy/'+arg.net+'_'+arg.dataset+ '_val', loss_per_epoch_val)
        np.save('acc_npy/'+arg.net+'_'+arg.dataset+ '_train', acc_per_epoch_train)
        np.save('acc_npy/'+arg.net+'_'+arg.dataset+ '_val', acc_per_epoch_val)
        
    # Testing
    print("Start Testing for Best Model")
    logger.info("Start Testing for Best Model")
    if os.path.isfile(model_path):
        model.load_state_dict(torch.load(model_path))
    # switch to evaluate mode
    model.eval()
    correct, ave_loss = 0, 0
    for batch_x, batch_y in dataset_test.batches(batch_size):
        # for gpu mode
        if arg.useGPU_f:
            input, target = Variable(batch_x, volatile=True).cuda(), Variable(batch_y, volatile=True).cuda()
        # for cpu mode
        else:
            input, target = Variable(batch_x, volatile=True), Variable(batch_y, volatile=True)

        # use cross entropy loss
        criterion = nn.CrossEntropyLoss()

        outputs = model(input)
        loss = criterion(outputs, target)
        _, pred_label = torch.max(outputs.data, 1)
        correct += (pred_label == target.data).sum()
        ave_loss += loss.data[0]

    accuracy = 1.0*correct*1.0/dataset_test.size()
    print(dataset_test.size())
    ave_loss /= dataset_test.size()
    print('==>>> test loss:{}, accuracy:{}'.format(ave_loss, accuracy))
    logger.info('==>>> test loss:{}, accuracy:{}'.format(ave_loss, accuracy))
        
if __name__ == "__main__":
    main()