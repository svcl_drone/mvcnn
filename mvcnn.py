import torch.nn as nn
import torch.nn.functional as F
from torch.nn import init
import torch
from torchvision.models.alexnet import model_urls
from torchvision.models.alexnet import model_urls as model_url_alexnet
from torchvision.models.vgg import model_urls as model_url_vgg
import torchvision.models as models

# base model
class AlexNet(nn.Module):
    def __init__(self):
        super(AlexNet, self).__init__()
        model_url_alexnet['alexnet'] = model_url_alexnet['alexnet'].replace('https://', 'http://')
        model = models.alexnet(pretrained=True)
        self.features = model.features
        self.fc1 = nn.Linear(9216,4096)
    def forward(self, x):        
        x = self.features(x)        
        x = x.view(-1, 9216)        
        x = self.fc1(x)        
        return x
    
# use element wise max pool during the view pooling stage
class mvcnn_Alex_pool(nn.Module):
    def __init__(self, input_view, output_class):
        super(mvcnn_Alex_pool, self).__init__()        
        self.input_view = input_view
        self.net = AlexNet()
        self.input_view = input_view
        self.output_class = output_class
        self.classifier = nn.Sequential(
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, self.output_class),
        )
    def forward(self, x):
        # x: (n, self.view, 3, 224, 224) 
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.max(merge_result,view_feature)
                
        merge_result = merge_result.view(merge_result.size(0), -1)        
        merge_result = self.classifier(merge_result)
        return merge_result

# use avg during the view pooling stage
class mvcnn_Alex_avg(nn.Module):
    def __init__(self, input_view, output_class):
        super(mvcnn_Alex_avg, self).__init__()        
        self.input_view = input_view
        self.net = AlexNet()
        self.input_view = input_view
        self.output_class = output_class

        self.classifier = nn.Sequential(
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, self.output_class),
        )

    def forward(self, x): 
        # x: (n, self.view, 3, 224, 224)     
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.cat([merge_result, view_feature],1)
                
        # append and shrink down number of features
        merge_result = torch.mean(merge_result,1)        
        merge_result = merge_result.view(merge_result.size(0), -1)        
        merge_result = self.classifier(merge_result)
        return merge_result    
    
# base model
class VGG(nn.Module):
    def __init__(self):
        super(VGG, self).__init__()
        model_url_vgg['vgg16'] = model_url_vgg['vgg16'].replace('https://', 'http://')
        model = models.vgg16(pretrained=True)
        self.features = model.features
        self.fc1 = nn.Linear(25088,4096)
    def forward(self, x):        
        x = self.features(x)        
        x = x.view(-1, 25088)        
        x = self.fc1(x)        
        return x
    
# use element wise max pool during the view pooling stage
class mvcnn_VGG_pool(nn.Module):
    def __init__(self, input_view, output_class):
        super(mvcnn_VGG_pool, self).__init__()        
        self.input_view = input_view
        self.net = VGG()
        self.input_view = input_view
        self.output_class = output_class
        self.classifier = nn.Sequential(
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, self.output_class),
        )
        
    def forward(self, x):
        # x: (n, self.view, 3, 224, 224)        
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.max(merge_result,view_feature)
            
        merge_result = merge_result.view(merge_result.size(0), -1)        
        merge_result = self.classifier(merge_result)
        return merge_result

# use avg during the view pooling stage
class mvcnn_VGG_avg(nn.Module):
    def __init__(self, input_view, output_class):
        super(mvcnn_VGG_avg, self).__init__()        
        self.input_view = input_view
        self.net = VGG()
        self.input_view = input_view
        self.output_class = output_class
        self.classifier = nn.Sequential(
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, 4096),
            nn.ReLU(),
            nn.Linear(4096, self.output_class),
        )
        
    def forward(self, x): 
        # x: (n, self.view, 3, 224, 224)     
        merge_result = None
        for view in range(self.input_view):
            view_feature = self.net(x[:,view]).view(x.shape[0],1,-1)
            if merge_result is None:
                merge_result = view_feature
            else:
                merge_result = torch.cat([merge_result, view_feature],1)
                
        # append and shrink down number of features
        merge_result = torch.mean(merge_result,1)        
        merge_result = merge_result.view(merge_result.size(0), -1)        
        merge_result = self.classifier(merge_result)
        return merge_result  